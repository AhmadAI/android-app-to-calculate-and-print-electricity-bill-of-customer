package com.example.q23;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.q23.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button btn;
    EditText etext1, etext2, etext3, etext4;
    TextView text2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn =findViewById(R.id.btn);
        etext1 = findViewById(R.id.etext1);
        etext2 = findViewById(R.id.etext2);
        etext3 = findViewById(R.id.etext3);
        etext4 = findViewById(R.id.etext4);
        text2 = findViewById(R.id.text2);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String id = etext1.getText().toString();
                String name = etext2.getText().toString();
                float unit = Float.parseFloat(etext3.getText().toString());
                float outstanding = Float.parseFloat(etext4.getText().toString());

                double balance = (unit)*(22.5)+(outstanding);

                text2.setText("Electricity bill"+"\nCustomer name: "+name+"\nCustomer id: "+id+"\nUnit consumed: "+unit+"\nOutstanding balance: "+outstanding+"\nTotal amount to be paid: "+balance);


            }
        });


    }
}